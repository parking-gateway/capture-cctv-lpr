package models

type ConfigCapture struct {
	UniqueID   string `json:"uniqueId" validate:"required"`
	TerminalID string `json:"terminalId" validate:"required"`
	OuCode     string `json:"ouCode" validate:"required"`
}

type ConfigCamera struct {
	Data []CameraList `json:"data"`
}

type CameraList struct {
	ID      string          `json:"ID"`
	CamList []ConfigCamList `json:"camList"`
}

type ConfigCamList struct {
	Title    string `json:"title"`
	URL      string `json:"url"`
	Gate     string `json:"gate"`
	Username string `json:"username"`
	Password string `json:"password"`
	Boundary string `json:"boundary"`
}
