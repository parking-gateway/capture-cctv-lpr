package models

import "time"

type Response struct {
	StatusCode       string      `json:"statusCode"`
	Success          bool        `json:"success"`
	ResponseDatetime time.Time   `json:"responseDatetime"`
	Result           interface{} `json:"result"`
	Message          string      `json:"message"`
}

type CaptureCCTV struct {
	VehicleNumber string  `json:"vehicleNumber"`
	VehicleSign   *string `json:"vehicleSign"`
	UTC           *string `json:"utc"`
	PlateColor    *string `json:"plateColor"`
}
