package tests

import (
	"fmt"
	"math"
	"testing"
	"time"
)

func TestTime(tst *testing.T) {
	currentTime := time.Now()
	format := currentTime.Format("2006-01-02 15:04:05")

	timeIN, _ := time.Parse("2006-01-02 15:04:05", "2021-06-01 08:00:00")
	timeOUT, _ := time.Parse("2006-01-02 15:04:05", format)
	calTime := (timeOUT.Unix()-timeIN.Unix())/60

	fmt.Println("CHECK IN", timeIN.Unix())
	fmt.Println("CHECK OUT", timeOUT.Unix())
	fmt.Println(math.Trunc(float64(calTime)))

	tm := time.Unix(timeOUT.Unix(), 0).UTC()
	fmt.Println(tm)

	t1 := time.Date(timeOUT.Year(), time.May, timeOUT.Day(), timeOUT.Hour(), timeOUT.Hour(), timeOUT.Second(), 0, time.UTC)
	t2 := time.Date(timeIN.Year(), time.May, timeIN.Day(), timeIN.Hour(), timeIN.Hour(), timeIN.Second(), 0, time.UTC)

	hs := t1.Sub(t2).Hours()

	hs, mf := math.Modf(hs)
	ms := mf * 60

	ms, sf := math.Modf(ms)
	ss := sf * 60

	fmt.Println(hs, "hours", ms, "minutes", ss, "seconds")
}