package services

import (
	"cctvcapturelpr/utils"
	"database/sql"
	"github.com/go-redis/redis"
)

type UsecaseService struct {
	RepoDB       *sql.DB
	RedisClient  *redis.Client
	CloudStorage *utils.ClientUploader
}

func NewUsecaseService(
	repoDB *sql.DB,
	RedisClient *redis.Client,
	CloudStorage *utils.ClientUploader,
) UsecaseService {
	return UsecaseService{
		RepoDB:       repoDB,
		RedisClient:  RedisClient,
		CloudStorage: CloudStorage,
	}
}
