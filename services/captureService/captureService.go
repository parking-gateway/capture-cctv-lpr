package captureService

import (
	"bufio"
	"cctvcapturelpr/config"
	"cctvcapturelpr/models"
	"cctvcapturelpr/services"
	"cctvcapturelpr/utils"
	"encoding/json"
	"fmt"
	"github.com/icholy/digest"
	"log"
	"mime/multipart"
	"net/http"
	"strings"
)

type CaptureService struct {
	Service services.UsecaseService
}

func NewCaptureService(service services.UsecaseService) CaptureService {
	return CaptureService{Service: service}
}

//func (svc captureService) CaptureRtsp(ctx echo.Context) error {
//	var result models.Response
//
//	request := new(models.ConfigCapture)
//	if err := helpers.BindValidateStruct(ctx, request); err != nil {
//		result = helpers.ResponseJSON(false, constans.VALIDATE_ERROR_CODE, err.Error(), nil)
//		return ctx.JSON(http.StatusBadRequest, result)
//	}
//
//	log.Println("INCOMING REQUEST : ", request)
//	var data []models.ConfigCamList
//	_ = json.Unmarshal([]byte(svc.Service.RedisClient.Get(request.TerminalID).Val()), &data)
//
//	go func() {
//		for _, row := range data {
//			filename := fmt.Sprintf("%s_%s_%s.%s", row.Title, request.UniqueID, row.Gate, "jpg")
//			fullPathImg := filepath.Join("assets", "images", filename)
//			log.Println("Output Image DIR:", fullPathImg)
//
//			log.Println("URL : ", row.URL)
//			_, err := exec.Command("ffmpeg", "-rtsp_transport", "tcp", "-i", row.URL, "-f", "image2", "-vf", "fps=1", "-frames:v", "1", "-atomic_writing", "1", fullPathImg).CombinedOutput()
//			if err != nil {
//				log.Println("Error Capture:", err.Error())
//				return
//			}
//
//			// Upload image to cloud storage server
//			if config.UPLOADImgGcp == constans.YES {
//				fl, err := os.Open(fullPathImg)
//				if err != nil {
//					panic(fmt.Sprintf("cannot open file: %v", err))
//				}
//				defer fl.Close()
//
//				err = svc.Service.CloudStorage.UploadFile(fl, fl.Name(), request.OuCode)
//				if err != nil {
//					fmt.Println("Error Upload File:", err.Error())
//				}
//			}
//
//		}
//	}()
//
//	result = helpers.ResponseJSON(true, constans.SUCCESS_CODE, constans.EMPTY_VALUE, nil)
//	return ctx.JSON(http.StatusOK, result)
//}

func (svc CaptureService) CaptureLPR() error {
	var captureLpr models.CaptureCCTV

	var data []models.ConfigCamList
	_ = json.Unmarshal([]byte(svc.Service.RedisClient.Get(config.TID).Val()), &data)

	go func() {
		for _, row := range data {
			tr := &digest.Transport{
				Username: row.Username,
				Password: row.Password,
			}

			client := &http.Client{Transport: tr}

			resp, err := client.Get(row.URL)
			if err != nil {
				log.Fatal(err)
			}
			defer resp.Body.Close()

			// untuk menandai pemisahnya
			reader := multipart.NewReader(resp.Body, row.Boundary)
			for {
				part, err := reader.NextPart()
				if err != nil {
					log.Fatal(err)
				}
				contentType := part.Header.Get("Content-Type")
				if contentType == "image/jpeg" {
					continue
				}
				// var count int

				// baca perbaris
				scanner := bufio.NewScanner(part)
				var result []string
				for scanner.Scan() {
					//baca semua baris
					textData := scanner.Text()
					if textData == "Heartbeat" {
						continue

					} else {
						if strings.Contains(textData, ".PlateNumber=") {
							res := strings.Split(textData, "=")
							captureLpr.VehicleNumber = res[1]
							result = append(result, res[1])
						} else if strings.Contains(textData, ".VehicleSign=") {
							res := strings.Split(textData, "=")
							captureLpr.VehicleSign = &res[1]
							result = append(result, res[1])
						} else if strings.Contains(textData, ".UTC=") {
							res := strings.Split(textData, "=")
							captureLpr.UTC = &res[1]
							result = append(result, res[1])
						} else if strings.Contains(textData, ".PlateColor=") {
							res := strings.Split(textData, "=")
							captureLpr.PlateColor = &res[1]
							result = append(result, res[1])
						}
					}
					// count++
				}
				if len(result) == 0 {
					continue
				} else {
					//resultStr := strings.Join(result, ",")
					byteRequest, _ := json.Marshal(captureLpr)
					channelsLprRedis := fmt.Sprintf("%s-%s", config.TID, "LPR")
					//svc.Service.RedisClient.Set(channelsLprRedis, byteRequest, 5*time.Minute)
					statusRedis := svc.Service.RedisClient.Publish(channelsLprRedis, byteRequest)
					log.Println("Status Redis : ", utils.ToString(statusRedis))
				}

			}
		}
	}()
	return nil
}
