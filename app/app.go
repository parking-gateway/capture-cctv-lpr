package app

import (
	"cctvcapturelpr/repositories"
	"cctvcapturelpr/services"
	"cctvcapturelpr/utils"
	"database/sql"
	"github.com/go-redis/redis"
)

func SetupApp(DB *sql.DB, repo repositories.Repository, redis *redis.Client, cloudStorage *utils.ClientUploader) services.UsecaseService {

	// Services
	usecaseSvc := services.NewUsecaseService(DB, redis, cloudStorage)

	return usecaseSvc
}
