package main

import (
	"cctvcapturelpr/app"
	"cctvcapturelpr/config"
	"cctvcapturelpr/helpers"
	"cctvcapturelpr/models"
	"cctvcapturelpr/repositories"
	"cctvcapturelpr/services/captureService"
	"cctvcapturelpr/utils"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-playground/locales/id"
	ut "github.com/go-playground/universal-translator"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gopkg.in/go-playground/validator.v9"
	id_translations "gopkg.in/go-playground/validator.v9/translations/id"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
)

//CustomValidator adalah
type CustomValidator struct {
	validator  *validator.Validate
	translator ut.Translator
}

// Passing Variable
var (
	uni         *ut.UniversalTranslator
	echoHandler echo.Echo
	ctx         = context.Background()
)

// Custom Validator and translation
func (cv *CustomValidator) Validate(i interface{}) error {
	err := cv.validator.Struct(i)
	if err != nil {
		errs := err.(validator.ValidationErrors)
		for _, row := range errs {
			return errors.New(row.Translate(cv.translator))
		}
	}

	return cv.validator.Struct(i)
}

func main() {

	echoHandler.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowCredentials: true,
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))

	echoHandler.HTTPErrorHandler = func(err error, c echo.Context) {
		report, ok := err.(*echo.HTTPError)
		if !ok {
			report = echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		result := helpers.ResponseJSON(false, strconv.Itoa(report.Code), err.Error(), nil)
		c.Logger().Error(report)
		c.JSON(report.Code, result)
	}

	rdsConnection := config.ConnectRedis()

	jsonFile, err := os.Open(filepath.Join("config.json"))
	if err != nil {
		fmt.Println(err)
	}

	log.Println("JSON FILE : ", jsonFile)
	defer jsonFile.Close()
	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		log.Println("Err Read Json file: ", err.Error())
		return
	}

	log.Println("BYTE VALUE :", byteValue)
	var data []models.CameraList
	err = json.Unmarshal(byteValue, &data)
	if err != nil {
		log.Println("Err Unmarshall : ", err.Error())
		return
	}

	log.Println("CAMERA LIST :", data)
	for _, rows := range data {
		status := rdsConnection.Set(rows.ID, utils.ToString(rows.CamList), 0)
		if status.Err() != nil {
			log.Println("Error Set Redis : ", status.Err())
		}
	}

	//client, err := storage.NewClient(context.Background())
	//if err != nil {
	//	log.Fatalf("Failed to create client: %v", err)
	//}
	//
	//cloudStorage := utils.NewCloudStorage(client, config.PROJECTId, config.BUCKETName, "")

	// Configuration Repository
	repo := repositories.NewRepository(nil, nil, ctx, nil)

	// Configuration Repository and Services
	services := app.SetupApp(nil, repo, rdsConnection, nil)
	captureSvc := captureService.NewCaptureService(services)

	go streamCaptureCCTVLPR(captureSvc)

	boardingService()

	select {}

}

func streamCaptureCCTVLPR(captureSvc captureService.CaptureService) {
	err := captureSvc.CaptureLPR()
	if err != nil {
		log.Println("Error Sync Data Failed:", err.Error())
	}

}

func init() {

	e := echo.New()
	echoHandler = *e
	validateCustom := validator.New()

	id := id.New()
	uni = ut.New(id, id)
	trans, _ := uni.GetTranslator("id")
	id_translations.RegisterDefaultTranslations(validateCustom, trans)
	e.Validator = &CustomValidator{validator: validateCustom, translator: trans}

	e.Static("/img/*", "assets/images")
	e.Use(middleware.Recover())
	e.Use(middleware.Secure())
	echoHandler.Use(middleware.Logger())
}

func boardingService() {
	fmt.Println(`
  __  __   _  __  _____      __  __ c          _      _   _             _____                         _               
 |  \/  | | |/ / |  __ \    |  \/  |         | |     (_) | |           / ____|                       (_)              
 | \  / | | ' /  | |__) |   | \  / |   ___   | |__    _  | |   ___    | (___     ___   _ __  __   __  _    ___    ___ 
 | |\/| | |  <   |  ___/    | |\/| |  / _ \  | '_ \  | | | |  / _ \    \___ \   / _ \ | '__| \ \ / / | |  / __|  / _ \
 | |  | | | . \  | |        | |  | | | (_) | | |_) | | | | | |  __/    ____) | |  __/ | |     \ V /  | | | (__  |  __/
 |_|  |_| |_|\_\ |_|        |_|  |_|  \___/  |_.__/  |_| |_|  \___|   |_____/   \___| |_|      \_/   |_|  \___|  \___|`)
}
