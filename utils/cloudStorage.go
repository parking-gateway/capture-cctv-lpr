package utils

import (
	"cloud.google.com/go/storage"
	"context"
	"fmt"
	"io"
	"io/fs"
	"log"
	"strings"
	"time"
)

type ClientUploader struct {
	Client     *storage.Client
	ProjectID  string
	BucketName string
	UploadPath string
}

func NewCloudStorage(client *storage.Client, projectId string, bucketName string, uploadPath string) ClientUploader {
	return ClientUploader{Client: client, ProjectID: projectId, BucketName: bucketName, UploadPath: uploadPath}

}

// UploadFile uploads an object
func (c *ClientUploader) UploadFile(file fs.File, object string, subUploadPath ...string) error {
	log.Println("Upload file to cloud storage:", object)
	ctx := context.Background()

	ctx, cancel := context.WithTimeout(ctx, time.Second*50)
	defer cancel()

	obj := strings.Split(object, "/")
	newPath := fmt.Sprintf("%s%s", c.UploadPath, obj[len(obj)-1])
	if len(subUploadPath) > 0 {
		newPath = fmt.Sprintf("%s%s/%s", c.UploadPath, subUploadPath[0], obj[len(obj)-1])
	}

	// Upload an object with storage.Writer.
	wc := c.Client.Bucket(c.BucketName).Object(newPath).NewWriter(ctx)
	if _, err := io.Copy(wc, file); err != nil {
		return err
	}
	if err := wc.Close(); err != nil {
		return err
	}

	return nil
}
