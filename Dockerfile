#!/bin/sh
FROM golang:1.21 as builder
MAINTAINER MKP Mobile Production <mkpproduction@gmail.com>#RUN apk add --no-cache git
ENV GIT_TERMINAL_PROMPT=1 GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GOPRIVATE="gitlab.com/mkpproductionteam/*,gitlab.com/mkp-apps2pay/*,gitlab.com/mkpproduction/*"
RUN echo "machine gitlab.com login hanserhanto password glpat-dxossiS26r7ei8SeV_YG" > ~/.netrc

RUN mkdir -p /app
WORKDIR /app
#ADD . /app
COPY . .

RUN rm -r go.mod go.sum
RUN go mod init cctvcapturelpr
RUN go mod tidy -e
RUN go build ./main.go

FROM alpine:3.13.1
WORKDIR /app
RUN touch .env

ENV TZ="Asia/Jakarta"
RUN apk add --no-cache tzdata
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


COPY --from=builder /app/main .
CMD ["/app/main"]
