package constans

const (
	SUCCESS_CODE = "101"
	PENDING_CODE = "102"
	FAILED_CODE  = "103"

	// Error berhubungan dengan data
	DATA_NOT_FOUND_CODE = "201"
	VALIDATE_ERROR_CODE = "202"

	SYSTEM_ERROR_CODE    = "501"
	UNDEFINED_ERROR_CODE = "502"

	EMPTY_VALUE = ""

	PRODUCT_COLLECTION = "product_col"
	GET_KEY            = "testjwt"
	TEMP_DIR_ZIP       = "assets/tmp"
	YES                = "YES"
	NO                 = "NO"
	TYPE_HIKVISION     = "HIKVISION"
)
